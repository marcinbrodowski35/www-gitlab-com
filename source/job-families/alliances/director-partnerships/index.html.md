---
layout: job_family_page
title: "Director of Partnerships"
---

## Responsibilities

* Identify and cultivate strategic relationships with current and potential
technology partners and large enterprises.
* Develop (and act on) collaboration plans for key partners, and develop product
adoption and customer success plans.
* Take overall responsibility for managing the strategic relationship (communications, expectations).
* Act as a liaison between internal teams at GitLab and strategic partners to communicate about feature requests, product roadmap, and how the alliance influences timing and direction.
* Understand the relative strength of competitors within strategic partners, if any.

## Requirements

* You know the DevOps software ecosystem like the back of your hand.
* You’re comfortable with code, and enjoy talking with developers and engineering directors alike.
* You’ve shown that you can build long term (international) strategic relationships, preferably involving open source software.
* You share our [values](/handbook/values), and work in accordance with those values.
* [Leadership at GitLab](https://about.gitlab.com/handbook/leadership/#director-group)
