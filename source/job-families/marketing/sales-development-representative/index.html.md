---
layout: job_family_page
title: "Sales Development Representative"
---

If you love talking about tech, learning about the latest and greatest, and want to continue to grow your career in software sales, GitLab's Sales Development Rep role could be a role worth exploring for you. In addition to having an affinity for tech and experience in sales, we want to build a team that thrives on asking questions, addressing challenges, and nurturing customers.
A successful SDR will take initiative to meet sales goals, qualify leads, build positive and lasting relationships with prospective clients, and support future and existing clients in their adoption of GitLab. Most importantly, our marketing team, including SDRs will use GitLab itself to document and update sales best practices and the company [handbook](/handbook).
As part of our marketing organization, the SDR will report to the Territory SDR Manager, and will work very closely with both Strategic Account Executives and Regional Sales Directors.

### Requirements
* Twelve months minimum experience in sales or client success in a software/tech company
* Excellent spoken and written English
* Experience in sales, marketing, or customer service
* Experience with CRM and/or marketing automation software
* An understanding of B2B software sales, open source software, and the developer product space
* Affinity for software and knowledge of the software development process
* Ability to carefully listen
* Desire for a continued career in software sales
* Passionate about technology and strong desire to learn
* Be ready to learn how to use GitLab and Git
* You share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values
* If in EMEA, fluency in spoken and written German and/or other European languages is preferred
* If in APAC, fluency in Mandarin or literacy in Simplified Chinese is required
* If in LATAM, fluency in Portuguese and Spanish is required

### Responsibilities
* Meet or exceed Sales Development Representative (SDR) sourced Sales Accepted Opportunity (SAO) volume targets.
* Identify and create new qualified opportunities from inbound [MQLs](https://about.gitlab.com/handbook/business-ops/#mql-definition) 
* Be able to identify where a prospect is in the sales and marketing funnel and nurture appropriately 
* Participate in documenting all processes in the handbook and update as needed with our business development manager
* Be accountable for your lead data and prospecting activities, log everything, take notes early and often
* Enhance prospective customer GitLab trial experiences through tailored and timely outreach
* Engage in conversations with prospective GitLab customers via live chat 
* Speed to lead; maintain [1 day SLA](https://about.gitlab.com/handbook/business-ops/#contact-requests) for all contact requests
* Discretion to qualify and disqualify leads when appropriate
* Develop and execute nurture sequences
* Work with designated strategic account leader to identify and prioritize key accounts to develop
* Working with the sales team to identify net-new opportunities and nurture existing opportunities within target accounts.
* Work in collaboration with field and corporate marketing to drive attendance at marketing events
* Attend field marketing events to engage with participants identify opportunities, and to schedule meetings
* Act as a mentor for new SDR hires in helping them navigate their key accounts.
* Work in collaboration with Digital Marketing to develop targeted marketing tactics against your assigned target accounts.

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/company/team).

* Qualified candidates receive a short questionnaire from our Global Recruiters
* Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first interview with the SDR Manager for the region they have applied for
* Candidates will complete a writing assessment where they will choose between two companies, decide which personas to target, and create sample introductory emails.
* Candidates will perform a mock role play call with either the SDR Manager or Team Lead focusing on one of the prospects they selected during the writing assessment. 
* Should the candidiates move forward after the mock role play, they will then be invited to schedule an interview with our Regional Sr. Manager or Director of Revenue Marketing
* Successful candidates will subsequently be made an offer

Additional details about our process can be found on our [hiring page](/handbook/hiring).
